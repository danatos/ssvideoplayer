//
//  SSNavigationPanelView.m
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 5/1/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import "SSNavigationPanelView.h"
#import "NSBundle+IDPExtensions.h"

@interface SSNavigationPanelView ()

- (void)animationWithDuration:(CGFloat)duration
                    withAplha:(CGFloat)alpha
        withCompletionHandler:(void (^)(BOOL finished))completionBlock;

@end

@implementation SSNavigationPanelView

#pragma mark -
#pragma mark Public

- (void)showNavigationPanelOnView:(UIView *)view {
//    NSString *className = NSStringFromClass([self class]);
//    SSNavigationPanelView *navigationPanel = [NSBundle loadClass:[self class] fromNibNamed:className owner:nil];
//    [view addSubview:navigationPanel];
}

- (void)hideNavigationPanel {
    
}

#pragma mark -
#pragma mark NSCoder

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupFunc];
    }
    return self;
}


#pragma mark -
#pragma mark Private

- (void)animationWithDuration:(CGFloat)duration
                    withAplha:(CGFloat)alpha
        withCompletionHandler:(void (^)(BOOL finished))completionBlock {
    
}

- (void)setupFunc {
    [[NSBundle mainBundle] loadNibNamed:@"SSNavigationPanelView" owner:self options:nil];
    [self addSubview:self.view];
}


@end
