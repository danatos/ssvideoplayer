//
//  SSNavigationPanelView.h
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 5/1/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSNavigationPanelView : UIView
@property (nonatomic, strong) IBOutlet UIView *view;
@property (nonatomic, assign, getter=isHidden) BOOL hideen;

@property (nonatomic, weak) IBOutlet UIProgressView *progressBar;

@property (nonatomic, weak) IBOutlet UIButton *tvChannelsButton;
@property (nonatomic, weak) IBOutlet UIButton *movieListButton;
@property (nonatomic, weak) IBOutlet UIButton *volumeButton;

@property (nonatomic, weak) IBOutlet UIButton *previousMovieButton;
@property (nonatomic, weak) IBOutlet UIButton *backwardPositionButton;
@property (nonatomic, weak) IBOutlet UIButton *playButton;
@property (nonatomic, weak) IBOutlet UIButton *fastForwardButton;
@property (nonatomic, weak) IBOutlet UIButton *nextMovieButton;

@property (nonatomic, weak) IBOutlet UIButton *hdSwitcher;

- (void)showNavigationPanelOnView:(UIView *)view;
- (void)hideNavigationPanel;

@end
