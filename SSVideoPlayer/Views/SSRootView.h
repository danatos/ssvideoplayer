//
//  SSMainView.h
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 4/25/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SSNavigationPanelView;

@interface SSRootView : UIView

@property (nonatomic, weak) IBOutlet UIView                 *filmArea;
@property (nonatomic, weak) IBOutlet SSNavigationPanelView  *panelView;

@property (nonatomic, weak) IBOutlet UIButton *playButton;
@property (nonatomic, weak) IBOutlet UIButton *stopButton;

@end
