//  SSMovieModel.h
//  SSVideoPlayer

#import "IDPModel.h"

@interface SSMovieModel : IDPModel

@property (nonatomic, readonly) NSString        *name;
@property (nonatomic, readonly) NSDictionary    *movieDescripion;
@property (nonatomic, readonly) NSURL           *movieURL;

+ (instancetype)modelWithURL:(NSString *)movieURL;

- (BOOL)addMovieInformation:(id)info withKey:(NSString *)key;

@end
