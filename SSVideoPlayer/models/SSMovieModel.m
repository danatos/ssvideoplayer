//  SSMovieModel.m
//  SSVideoPlayer

#import "SSMovieModel.h"
#import "NSObject+IDPExtensions.h"

static NSString *const kSSBasicMovieName = @"The movie";
static NSString *const kSSBasicMovieURL  = @"http://testapi.qix.sx/video/sky.mp4";

static NSString *const kSSMovieNameKey = @"name";
static NSString *const kSSMovieURLKey  = @"URL";

@interface SSMovieModel ()
@property (nonatomic, strong) NSMutableDictionary   *mutableMovieDescripion;
@property (nonatomic, strong) NSMutableString       *mutableName;
@property (nonatomic, strong) NSURL                 *movieURL;

- (NSString *)movieName:(NSString *)movieURL;
- (void)fillMoviePrimaryDecsription;

@end

@implementation SSMovieModel

@dynamic movieDescripion;
@dynamic name;

#pragma mark -
#pragma mark Class Method 

+ (instancetype)modelWithURL:(NSString *)movieURL {
    NSString *URLString = movieURL;
    SSMovieModel *model = [SSMovieModel object];
    model.movieURL = [NSURL URLWithString:URLString];
    [model.mutableName setString:[model movieName:URLString]];
    
    return model;
}

#pragma mark -
#pragma mark Initialization and Dealoccation

- (instancetype)init {
    self = [super init];
    if (self) {
        self.movieURL = [NSURL URLWithString:kSSBasicMovieURL];
        self.mutableMovieDescripion = [NSMutableDictionary dictionary];
        self.mutableName = [NSMutableString stringWithString:kSSBasicMovieName];
    }
    
    return self;
}

#pragma mark -
#pragma mark Acessors

- (NSDictionary *)movieDescripion {
    return [self.mutableMovieDescripion copy];
}

- (NSString *)name {
    return [self.mutableName copy];
}

#pragma mark -
#pragma mark Public

- (BOOL)addMovieInformation:(id)info withKey:(NSString *)key {
    NSArray *keysArray = self.mutableMovieDescripion.allKeys;
    for (NSString *dictionatyKey in keysArray) {
        if ([key isEqualToString:dictionatyKey]) {
            
            return NO;
        } else {
            [self.mutableMovieDescripion setObject:info forKey:key];
        }
    }
    
    return YES;
}

#pragma mark -
#pragma mark Private

- (NSString *)movieName:(NSString *)movieURL {
    NSArray *sharedMovieURL = [movieURL componentsSeparatedByString:@"/"];
    NSString *movieName = [sharedMovieURL lastObject];
    
    return movieName;
}

- (void)fillMoviePrimaryDecsription {
    NSMutableDictionary *dictionary = self.mutableMovieDescripion;
    [dictionary setObject:self.name forKey:kSSMovieNameKey];
    [dictionary setObject:self.movieURL forKey:kSSMovieURLKey];
}
@end
