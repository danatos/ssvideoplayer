//  Created by Sergiy Shevchuk on 5/1/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.

#import "IDPModel.h"

@class SSMovieModel;

@interface SSModelsArray : IDPModel <IDPModelObserver>

@property (nonatomic, readonly) NSArray *data;

+ (instancetype)model;

- (void)addNewModel:(SSMovieModel *)theModel;
- (void)removeModelAtIndexPath:(NSUInteger)indexPath;

@end
