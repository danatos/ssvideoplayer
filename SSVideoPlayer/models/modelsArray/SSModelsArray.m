//  Created by Sergiy Shevchuk on 5/1/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.

#import "SSModelsArray.h"
#import "NSObject+IDPExtensions.h"

@interface SSModelsArray ()
@property (nonatomic, strong) NSMutableArray *mutableData;

@end

@implementation SSModelsArray

@dynamic data;

#pragma mark -
#pragma mark Class Methods

+ (instancetype)model {
    SSModelsArray *model = [SSModelsArray object];
    model.mutableData = [NSMutableArray array];
    
    return model;
}

#pragma mark -
#pragma mark Acessors

- (NSArray *)data {
    return [self.mutableData copy];
}

#pragma mark -
#pragma mark Public

- (void)addNewModel:(SSMovieModel *)theModel {
    [self.mutableData addObject:theModel];
} 

- (void)removeModelAtIndexPath:(NSUInteger)indexPath {
    [self.mutableData removeObjectAtIndex:indexPath];
}

#pragma mark -
#pragma mark IDPModelObserver Protocol Notifications

- (void)notifyObserversOfChanges {
    [self notifyObserversWithSelector:@selector(modelDidChange:)];
}

- (void)notifyObserversOfSuccessfulLoad {
    [self notifyObserversWithSelector:@selector(modelDidLoad:)];
}

@end
