//
//  SSRootViewController.h
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 4/25/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SSRootView;

@interface SSRootViewController : UIViewController

@property (nonatomic, readonly) SSRootView *rootView;

- (IBAction)playButton:(id)sender;
- (IBAction)stopButton:(id)sender;





@end
