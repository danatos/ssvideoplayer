//
//  SSRootViewController.m
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 4/25/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import "SSRootViewController.h"
#import "SSRootView.h"
#import "SSNavigationPanelView.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SSRootViewController ()

@property (nonatomic, strong) MPMoviePlayerController   *moviePlayer;
@property (nonatomic, strong) SSNavigationPanelView     *navigationPanelView;

@end

@implementation SSRootViewController

@dynamic rootView;

#pragma mark -
#pragma mark Initialization and Dealoccation

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      //  self.moviePlayer = [[MPMoviePlayerController alloc] init];
      //  self.navigationPanelView = [[SSNavigationPanelView alloc] init];
    }
    
    return self;
}

#pragma mark -
#pragma mark Acessor

- (SSRootView *)rootView {
    if ([self isViewLoaded] && [self.view isKindOfClass:[SSRootView class]]) {
        return (SSRootView *)self.view;
    }
    
    return nil;
}

- (IBAction)playButton:(id)sender {
    [self movieLoad];
    [self.moviePlayer play];
    //    [self.navigationPanelView showNavigationPanelOnView:self.rootView];
}

- (IBAction)stopButton:(id)sender {
    [self.moviePlayer stop];
}

#pragma mark -
#pragma mark Private

- (void)movieLoad {
    NSString *URLString = @"http://testapi.qix.sx/video/sky.mp4";
    NSURL *URL = [NSURL URLWithString:URLString];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:URL];
    MPMoviePlayerController *moviePlayer = self.moviePlayer;
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    [moviePlayer prepareToPlay];
    [moviePlayer.view setFrame:self.rootView.filmArea.bounds]; // from Apple docs
    [self.rootView.filmArea addSubview:moviePlayer.view];
}

#pragma mark -
#pragma mark UIViewController Life Time

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
