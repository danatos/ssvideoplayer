//
//  IDPLoadingView.h
//  ClipIt
//
//  Created by Oleksa 'trimm' Korin on 2/26/13.
//

#import <UIKit/UIKit.h>

@interface IDPLoadingView : UIView

+ (id)loadingViewInView:(UIView *)view;
+ (id)loadingViewInView:(UIView *)view withMessage:(NSString *)message;

@end
