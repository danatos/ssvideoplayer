//
//  AppDelegate.h
//  SSVideoPlayer
//
//  Created by Sergiy Shevchuk on 4/24/15.
//  Copyright (c) 2015 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

